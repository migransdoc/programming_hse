import csv


def reader(file):
    data = list()
    with open(file) as f:
        spam = csv.reader(f)
        next(spam)  # Пропускаем шапку таблицы
        for row in spam:
            data.append(row[1:7])  # Добавляем нужные колонки
    return data


def find_matches_by_team(team, data):
    ans = [['Date', 'HomeTeam', 'AwayTeam', 'HomeTeamGoals', 'AwayTeamGoals']]  # Делаем шапку
    for row in range(len(data)):
        if data[row][1] == team or data[row][2] == team:
            ans.append(data[row][:-1])  # Добавляем строку с командой
    if len(ans) > 1:  # Если такая команда есть и мы нашли данные
        max_len = max([len(str(e)) for r in ans for e in r]) + 4
        for row in ans:
            print(*list(map('{{:>{length}}}'.format(length=max_len).format, row)))  # Красивый вывод
        return 'Success\n'
    return 'No such team\n'  # Иначе пишем что такой команды нет


def find_matches_by_date(date, data):
    ans = [['Date', 'HomeTeam', 'AwayTeam', 'HomeTeamGoals', 'AwayTeamGoals']]
    for row in range(len(data)):
        if data[row][0] == date:
            ans.append(data[row][:-1])
    if len(ans) > 1:
        max_len = max([len(str(e)) for r in ans for e in r]) + 4
        for row in ans:
            print(*list(map('{{:>{length}}}'.format(length=max_len).format, row)))
        return 'Success\n'
    return 'No such date\n'


def ranking_table_overall(data):  # Подсчет общих результатов
    teams = set()
    for row in data:
        teams.add(row[1])
        teams.add(row[2])
    teams = list(teams)  # Список всех команд

    arr = []
    for team in teams:  # Пробегаемся по каждой команде и считаем результаты
        gamesPlayed = 0
        wins = 0
        draws = 0
        losses = 0
        goalsScored = 0
        goalDifference = 0
        points = 0
        arr.append([team])
        for row in data:
            if row[1] == team:
                gamesPlayed += 1
                goalsScored += int(row[3])
                goalDifference += int(row[3]) - int(row[4])
                if row[5] == 'H':
                    points += 3
                    wins += 1
                elif row[5] == 'D':
                    points += 1
                    draws += 1
                else:
                    losses += 1  # Присуждаем очки и другие данные

            elif row[2] == team:
                gamesPlayed += 1
                goalsScored += int(row[4])
                goalDifference += int(row[4]) - int(row[3])
                if row[5] == 'A':
                    points += 3
                    wins += 1
                elif row[5] == 'D':
                    points += 1
                    draws += 1
                else:
                    losses += 1

        arr[-1].append(gamesPlayed)
        arr[-1].append(wins)
        arr[-1].append(draws)
        arr[-1].append(losses)
        arr[-1].append(goalsScored)
        arr[-1].append(goalDifference)
        arr[-1].append(points)  # Все в 1 массив

    arr.sort(key=lambda x: x[7], reverse=True)  # сортируем по убыванию очков
    return arr


def ranking_table_England(data):  # Разбор спорных ситуаций для Англии
    arr = ranking_table_overall(data)

    team = 0
    place = 1
    while team < len(arr) - 1:  # Разбираем спорные ситуации равенства очков
        if arr[team][7] > arr[team + 1][7] or (
                arr[team][7] == arr[team + 1][7] and arr[team][6] > arr[team + 1][6]) or (
                arr[team][7] == arr[team + 1][7] and arr[team][6] == arr[team + 1][6] and arr[team][5] > arr[team + 1][
            5]):
            arr[team].insert(0, place)
            team += 1
            place += 1  # Если все хорошо

        elif arr[team][7] == arr[team + 1][7] and arr[team][6] == arr[team + 1][6] and arr[team][5] == arr[team + 1][5]:
            arr[team].insert(0, place)
            arr[team+1].insert(0, place)
            team += 1  # Случай когда у нескольких команд 1 место

        elif (arr[team][7] == arr[team + 1][7] and arr[team][6] < arr[team + 1][6]) or (
                arr[team][7] == arr[team + 1][7] and arr[team][6] == arr[team + 1][6] and arr[team][5] < arr[team+1][5]
        ):
            arr[team], arr[team + 1] = arr[team + 1], arr[team]  # Случай когда меняем команды местами
    arr[-1].insert(0, place)  # Последняя команда, по которой в цикле мы не прошлись

    arr.insert(0, ['Place', 'Team', 'GamesPlayed', 'Wins', 'Draws', 'Loses', 'Goals Scored', 'Goals Diff', 'Points'])
    # Заголовок
    max_len = max([len(str(e)) for r in arr for e in r]) + 2
    for row in arr:
        print(*list(map('{{:>{length}}}'.format(length=max_len).format, row)))  # Красивый вывод
    print()


def ranking_table_Italy(data):  # Разбор спорных ситуаций для Италии
    def head_to_head_recs(team1, team2, data):  # Разбор спора между 2 командами
        pointsTeam1 = pointsTeam2 = 0
        for row in data:
            if (row[1] == team1 and row[2] == team2 and row[5] == 'H') or (
                    row[2] == team1 and row[1] == team2 and row[5] == 'A'):
                pointsTeam1 += 3
            elif (row[1] == team1 and row[2] == team2 and row[5] == 'D') or (
                    row[2] == team1 and row[1] == team2 and row[5] == 'D'):
                pointsTeam1 += 1
                pointsTeam2 += 1
            elif (row[1] == team1 and row[2] == team2 and row[5] == 'A') or (
                    row[2] == team1 and row[1] == team2 and row[5] == 'H'):
                pointsTeam2 += 3
        return pointsTeam1, pointsTeam2

    def head_to_head_goalsdiff(team1, team2, data):  # Дальшнейший разбор спора
        goalsTeam1 = goalsTeam2 = 0
        for row in data:
            if row[1] == team1 and row[2] == team2:
                goalsTeam1 += int(row[3]) - int(row[4])
                goalsTeam2 += int(row[4]) - int(row[3])
        return goalsTeam1, goalsTeam2

    arr = ranking_table_overall(data)

    team = 0
    place = 1
    while team < len(arr) - 1:  # Разбираем спорные ситуации равенства очков
        if arr[team][7] > arr[team + 1][7]:
            arr[team].insert(0, place)
            team += 1
            place += 1  # Если кол-во очков строго больше

        else:  # Разбираем спорные ситуации
            pointsTeam1, pointsTeam2 = head_to_head_recs(arr[team][0], arr[team+1][0], data)
            goalsTeam1, goalsTeam2 = head_to_head_goalsdiff(arr[team][0], arr[team+1][0], data)
            if pointsTeam1 > pointsTeam2 or (pointsTeam1 == pointsTeam2 and goalsTeam1 > goalsTeam2) or (
                    pointsTeam1 == pointsTeam2 and goalsTeam1 == goalsTeam2 and arr[team][6] > arr[team+1][6
            ]) or (pointsTeam1 == pointsTeam2 and goalsTeam1 == goalsTeam2 and arr[team][6] == arr[team+1][6
            ] and arr[team][5] > arr[team+1][5]):
                arr[team].insert(0, place)
                team += 1
                place += 1  # Разобрали ситуацию внутри 2 команд с равными очками

            elif pointsTeam1 == pointsTeam2 and goalsTeam1 == goalsTeam2 and arr[team][6] == arr[team + 1][
                6] and arr[team][5] == arr[team + 1][5]:
                arr[team].insert(0, place)
                arr[team + 1].insert(0, place)
                team += 1  # Случай когда у нескольких команд 1 место

            elif pointsTeam1 < pointsTeam2 or (pointsTeam1 == pointsTeam2 and goalsTeam1 < goalsTeam2) or (
                    pointsTeam1 == pointsTeam2 and goalsTeam1 == goalsTeam2 and arr[team][6] < arr[team + 1][6]) or (
                    pointsTeam1 == pointsTeam2 and goalsTeam1 == goalsTeam2 and arr[team][6] == arr[team + 1][
                6] and arr[team][5] < arr[team+1][5]):
                arr[team], arr[team+1] = arr[team+1], arr[team]  # Поменяли 2 команды местами
    arr[-1].insert(0, place)  # Последняя команда, по которой в цикле мы не прошлись

    arr.insert(0, ['Place', 'Team', 'GamesPlayed', 'Wins', 'Draws', 'Loses', 'Goals Scored', 'Goals Diff', 'Points'])
    # Заголовок
    max_len = max([len(str(e)) for r in arr for e in r]) + 2
    for row in arr:
        print(*list(map('{{:>{length}}}'.format(length=max_len).format, row)))  # Красивый вывод
    print()


files = ['E0 17 18.csv', 'E0 18 19.csv', 'E0 19 20.csv', 'I1 18 19.csv', 'I1 19 20.csv']

tournaments = dict()
for file in files:
    try:
        new_file = reader(file)
        tournaments[file] = new_file
    except FileNotFoundError: # Заглушка от отсутствия нужных файлов
        print('File {} doesnt exist\n'.format(file))


while True:  # Пользовательское меню
    print('Press 1 to load England Premier League Season 17/18')
    print('Press 2 to load England Premier League Season 18/19')
    print('Press 3 to load England Premier League Season 19/20')
    print('Press 4 to load Italy Serie A 18/19')
    print('Press 5 to load Italy Serie A 19/20')
    user = input('Press e to exit ')

    if user == '1' and 'E0 17 18.csv' in tournaments.keys():
        main_tour = 'E0 17 18.csv'
    elif user == '2' and 'E0 18 19.csv' in tournaments.keys():
        main_tour = 'E0 18 19.csv'
    elif user == '3' and 'E0 19 20.csv' in tournaments.keys():
        main_tour = 'E0 19 20.csv'
    elif user == '4' and 'I1 18 19.csv' in tournaments.keys():
        main_tour = 'I1 18 19.csv'
    elif user == '5' and 'I1 19 20.csv' in tournaments.keys():
        main_tour = 'I1 19 20.csv'
    elif user == 'e':
        exit()
    else:
        print('Incorrect input or absense of files\n')  # Заглушка
        continue

    curr_data = tournaments[main_tour]
    print('\nPress 1 to find all one team matches')
    print('Press 2 to find all matches for one date')
    print('Press 3 to see ranking table')
    user2 = input('Press e to exit ')
    if user2 == '1':
        team = input('\nEnter the team ')
        print(find_matches_by_team(team, curr_data))
    elif user2 == '2':
        date = input('\nEnter the date ')
        print(find_matches_by_date(date, curr_data))
    elif user2 == '3' and (user == '1' or user == '2' or user == '3'):
        ranking_table_England(curr_data)
    elif user2 == '3' and (user == '4' or user == '5'):
        ranking_table_Italy(curr_data)
    elif user2 == 'e':
        exit()
    else:
        print('Incorrect input\n')
